# keycloak-postgres-nginx

Docker configuration for Keycloak + Nginx + Postgres with Let's Encrypt support.

Easily setup a standalone node for [Keycloak](http://www.keycloak.org) server so you can worry less about authentication and worry more about your application features.

## Usage

1. Generate a customized image from nginx.
    * at localhost, dev purposes of course, inside the directory /path_to_your_repository/keycloak-nginx-self-ssl/ type `docker build -t geniusbr/keycloak-nginx-self-signed .`
    * at yout host with a domain, inside the directory /path_to_your_repository/keycloak-nginx-lets-encrypt/ type `docker build -t geniusbr/nginx_letsencrypt .`

1. Create a file `.env` based from `.env.example` and set the variables according with your environment and you want do it.

1. For `LE_OPTIONS`, choose either `--staging` to environment staging or `--keep-until-expiring`, to production

1. Bootstrapping the services:
    * For dev purposes: `docker-compose -f docker-compose-self-signed.yml up`
    * For production: `docker-compose -f docker-compose-lets-encrypt.yml up` It not works in localhost

## Credits

I would like say very very thanks to [Jonathan ES Lin](https://github.com/ernsheong) for creation the base this project in [here](https://github.com/ernsheong/keycloak-postgres-nginx).

## License

MIT License.
